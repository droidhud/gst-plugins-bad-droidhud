# gst-plugins-bad-droidhud

This is a horrible, horrible hack to extract parsed but undecoded video packets from a Gstreamer pipeline.  It is used by Droidhud so it can directly manage MediaCodec.

Proper implementations welcome.

## Build instructions

### Installing Cerbero

1. You may need to change /etc/lsb-release to a compatible release.  See below.
2. git cerbero git://anongit.freedesktop.org/gstreamer/cerbero
3. cerbero bootstrap
4. create android.cbc in .cerbero/, select armv7 and nodebug variant, set destination with prefix
5. See android.cbc below
6. cerbero -c ~/.cerbero/android.cbc bootstrap (to install NDK and other tools)
7. cd /home
8. mkdir -p slomo/cerbero/dist
9. ln -s /home/<you>/src/gstreamer-android /home/slomo/cerbero/dist/android_armv7

#### /etc/lsb-release

```
DISTRIB_ID=debian
DISTRIB_RELEASE=jessie
DISTRIB_CODENAME=jessie
DISTRIB_DESCRIPTION="jessie"
```
#### android.cbc

```
import os
from cerbero.config import Platform, Architecture, Distro, DistroVersion

target_platform = Platform.ANDROID
target_distro = Distro.ANDROID
target_distro_version = DistroVersion.ANDROID_GINGERBREAD
target_arch = Architecture.ARMv7

prefix='/home/joel/src/gstreamer-android'

variants = ['nodebug']
```

### Obtaining Gstreamer

#### Option A: Using pre-built binaries

1. Download gstreamer-1.0-android-armv7-1.6.3.tar.bz2  (or “nodebug”) verison)
2. Unpack to ~/src/gstreamer-1.0-android-armv7-1.6.3
3. ln -s /home/<you>/src/gstreamer-1.0-android-armv7-1.6.3 /home/<you>/src/gstreamer-android

#### Option B: Building from source

1. cerbero -c ~/.cerbero/android.cbc build gstreamer-1.0-static gst-libav-1.0-static gst-plugins-base-1.0-static gst-plugins-good-1.0-static gst-plugins-bad-1.0-static gst-plugins-ugly-1.0-static gst-android-1.0

### Obtaining and building a modified gst-plugins-bad

1. git clone git://anongit.freedesktop.org/gstreamer/gst-plugins-bad
2. cd gst-plugins-bad
3. cerbero -c ~/.cerbero/android.cbc shell
4. (first time) ./autogen.sh --noconfigure
5. ./configure --prefix /home/joel/src/gstreamer-android --libdir /home/joel/src/gstreamer-android/lib --enable-introspection=no --enable-examples=no --enable-static-plugins --disable-shared --enable-static --disable-gtk-doc --disable-gtk-doc --disable-gsm --disable-festival --disable-videomaxrate --disable-bz2 --disable-libde265 --disable-linsys --disable-fbdev --disable-apexsink --disable-celt --disable-curl --disable-dc1394 --disable-directfb --disable-dirac --disable-faac --disable-flite --disable-gme --disable-ladspa --disable-lv2 --disable-mimic --disable-modplug --disable-mpeg2enc --disable-mplex --disable-musepack --disable-mythtv --disable-neon --disable-ofa --disable-openal --disable-opencv --disable-pvr --disable-sdl --disable-sndfile --disable-soundtouch --disable-spandsp --disable-teletextdec --disable-timidity --disable-vdpau --disable-voamrwbenc --disable-wildmidi --disable-xvid --disable-zbar --disable-sdi --disable-qt --disable-maintainer-mode --disable-silent-rules --disable-introspection --disable-audiovisualizers --disable-liveadder --disable-openh264 --host=arm-linux-androideabi
6. make
7. make install
8. cd ~/src/gstreamer-android/lib/gstreamer-1.0
9. (ARGH) mv *.a *.la static

